const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require('path');
let sourcePath = "./app/web/static/";
let cmsSourcePath = "./app/admin/static/";

module.exports = [function initWebpack() {
    let config = {};

    config.entry = "./app/web/static/index.js";
    config.output = { filename: "./app/web/static/build/bundle.js" };
    config.devtool = "source-map";

    config.module = {
        rules: [{
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{
                            loader: "css-loader",
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                }),
                exclude: path.resolve(__dirname, "node_modules")
            },
            {
                test: /\.css$/,
                loader: ["style-loader", "css-loader"]
            },
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: path.resolve(__dirname, "node_modules"),
                options: {
                    presets: ["es2015"]
                }
            }
        ]
    };
    config.watch = true;
    config.plugins = [
        new ExtractTextPlugin(sourcePath + "build/bundle.css")
    ];

    config.devServer = {
        proxy: {
            "/app": 'http://127.0.0.1:5000',
            secure: false
        }
    };

    return config;
},
    function webpackAdminBuild() {
    let config = {};

    config.entry = "./app/admin/static/index.js";
    config.output = { filename: "./app/admin/static/build/bundle.js" };
    config.devtool = "source-map";

    config.module = {
        rules: [{
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{
                            loader: "css-loader",
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                }),
                exclude: path.resolve(__dirname, "node_modules")
            },
            {
                test: /\.css$/,
                loader: ["style-loader", "css-loader"]
            },
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: path.resolve(__dirname, "node_modules"),
                options: {
                    presets: ["es2015"]
                }
            }
        ]
    };
    config.watch = true;
    config.plugins = [
        new ExtractTextPlugin(cmsSourcePath + "build/bundle.css")
    ];

    config.devServer = {
        proxy: {
            "/app": 'http://127.0.0.1:5000',
            secure: false
        }
    };
    return config;
}];