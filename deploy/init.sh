#!/bin/bash
# Simple utility script to handle installation of deployment environment for Looks Inn

PROJECT_NAME="bns"

echo "Setting up Python virtual environment configuration"
VIRTUAL_ENVIRONMENT_PATH=/core/envs/bns

read -r -p "There exists a directory for $VIRTUAL_ENVIRONMENT_PATH. Do you wish to overwrite? (This is advised) (y/n): ." resp

if [[ ${resp} == "y" ]]; then
    if [ -d "$VIRTUAL_ENVIRONMENT_PATH" ]; then
        echo "There exists a directory for $VIRTUAL_ENVIRONMENT_PATH. Removing now."
        rm -r ${VIRTUAL_ENVIRONMENT_PATH}
    fi
    virtualenv ${VIRTUAL_ENVIRONMENT_PATH} -p python3.4
    source ${VIRTUAL_ENVIRONMENT_PATH}/bin/activate
    pip install -r /core/apps/bns/requirements.txt
    pip install uwsgi
    echo "Closing virtual environment with packages installed: "
    cat /core/apps/bns/requirements.txt
    deactivate
fi

read -r -p "Do you wish to automatically load NGINX server file for ${PROJECT_NAME} (y or n): " resp
if [[ ${resp} == "y" ]]; then
    echo "Cloning NGINX configuration files"
    cp /core/apps/bns/deploy/${PROJECT_NAME} /etc/nginx/sites-available/
    echo "Symlinking NGINX server file /etc/nginx/sites-available/${PROJECT_NAME} to /etc/nginx/sites-enabled"
    ln -s /etc/nginx/sites-available/${PROJECT_NAME} /etc/nginx/sites-enabled
fi

read -r -p "Do you wish to automatically load upstart script file for ${PROJECT_NAME} (y or n): " resp
if [[ ${resp} == "y" ]]; then
    echo "Cloning upstart conf file"
    cp /core/apps/bns/deploy/bns.conf /etc/init/
fi

echo "Creating default instance configuration file - please fill details"
mkdir /core/apps/bns/instance
touch /core/apps/bns/instance/config.cfg;

echo "Creating base database with ORM now"
PYTHONPATH=/core/apps/bns/ python /core/apps/bns/app/orm.py db init RELEASE
PYTHONPATH=/core/apps/bns/ python /core/apps/bns/app/orm.py db migrate RELEASE
PYTHONPATH=/core/apps/bns/ python /core/apps/bns/app/orm.py db upgrade RELEASE
