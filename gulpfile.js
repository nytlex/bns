const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const JSFolder = "app/web/static/js";

gulp.task('styles', function() {
    gulp.src('app/web/static/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sass({ style: 'expanded' }))
        .pipe(gulp.dest('app/web/static/css/'));
});

gulp.task('babel', function() {
    gulp.src(JSFolder + '/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest(JSFolder + '/build'));
});


//Watch task
gulp.task('default', function() {
    gulp.watch('app/web/static/scss/**/*.scss', ['styles']);
    gulp.watch(JSFolder + '/*.js', ['babel']);
});

