from werkzeug.exceptions import HTTPException
from jinja2.exceptions import TemplateNotFound
from flask import render_template

from app.errors.exceptions import ServerException
from app.errors import error_handler


@error_handler.app_errorhandler(HTTPException)
@error_handler.app_errorhandler(ServerException)
@error_handler.app_errorhandler(404)
@error_handler.app_errorhandler(429)
@error_handler.app_errorhandler(403)
def catch_error(error):
    code = 0
    if error == TemplateNotFound:
        code = 401
    template = 'pages/error/{}.html'.format(error.code)
    if error.code == 404:
        return render_template(template)
    elif error.code == 429:
        return error