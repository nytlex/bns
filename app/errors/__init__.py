__author__ = 'Keith Byrne'

from flask import Blueprint

error_handler = Blueprint(
    'errors',
    __name__,
    template_folder='/web/templates',
    static_folder='./static',
    static_url_path='/web/static'
)

from app.errors import errors