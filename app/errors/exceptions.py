__author__ = 'Keith Byrne'


DEFINITIONS = {
    404: 'The resource you requested cannot be found',
    500: 'An issue has occurred on the server. Please contact the site administrator if this issue persists.'
}


class ServerException(Exception):
    status_code = None

    def __init__(self, status_code=500, message=None, payload=None):
        self.status_code = status_code
        self.message = message
        self.payload = payload
        self.code = status_code

    def get_definition(self):
        return 'HTTP {} = {}'.format(self.code, DEFINITIONS[self.code])


class ViewInterfaceException(Exception):
    def __init__(self):
        self.message = 'This view requies a template in order to process correctly.'