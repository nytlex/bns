__author__ = 'Keith Byrne'


import sys
sys.path.append('/core/applications/bns/')

from flask_script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from app import create_app
from app.database import database

if sys.argv[3]:
    app = create_app(str(sys.argv[3]))
    del sys.argv[3]
else:
    app = create_app()

migrate = Migrate(app, database)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()