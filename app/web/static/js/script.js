import counter from "./counter";

$(document).ready(function() {
    $("#nav-icon").click(function () {
        $(this).toggleClass("nav-open");
        $("body").toggleClass("mobile-navigation");
    });
});