/**
 * Created by Keith Byrne on 18/10/2017.
 */

function counter() {
    let timer = undefined;
    try {
        timer = document.getElementById("counter").dataset.timer;
    } catch (Exception){
        return false;
    }
    function countdown (){
        timer = timer - 1;
        if (timer < 1) {
            window.location = "/";
        } else {
            document.getElementById("counter").innerHTML = timer;
            window.setTimeout(countdown, 1000);
        }
    }
    countdown();
}

export default counter();