__author__ = 'Keith Byrne'

from datetime import datetime
from jinja2 import filters


def datetime_filter(value: datetime) -> str:
    current_datetime = datetime.now()
    output_format = '%d/%m/%Y'
    day_delta = current_datetime - value
    return '{} - {} days old'.format(value.strftime(output_format), day_delta.days)


filters.FILTERS['datetime_filter'] = datetime_filter