__author__ = 'Keith Byrne'


class BaseTag(object):
    pass


class ListItem(BaseTag):

    def __init__(self):
        self.tag = 'li'
        self.markup = '<li class={css}><a href="{url}">{data}</a></li>'


class UnorderedList(BaseTag):
    def __init__(self):
        self.tag = 'ul'
        self.markup = '<ul class={css}>{data}</ul>'


class Anchor(BaseTag):
    def __init__(self):
        self.tag = 'a'
        self.markup = '<a href={tag} class={css}>{data}</ul>'