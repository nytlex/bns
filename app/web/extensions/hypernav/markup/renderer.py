__author__ = 'Keith Byrne'


class Renderer(object):

    def __init__(self):
        self.base = '<{tag}>'

    def build_html(self, html, data, css='', url=''):
        return html.format(css=css, data=data, url=url)

    def render_from_attributes(self, element, data=None, css=None, url=None):
        html = self.build_html(element.tag.markup, element.label, css)
        return html

    def _render_anchor(self):
        return '<a href="{url}">{data}</a>'