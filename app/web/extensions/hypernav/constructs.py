__author__ = 'Keith Byrne'

from flask import render_template

from app.web.extensions.hypernav.markup.tags import ListItem
from app.web.extensions.hypernav.markup.tags import UnorderedList


class NavItem(object):
    """
    Base class for all nav items. Each item stored within a nav is a nav item at its top level

    """
    active = False
    markup = None

    def render(self):
        return render_template(self)

    def set_markup(self, markup):
        self.markup = markup

    def draw(self):

        return str(self.markup)


class Link(NavItem):

    def __init__(self, label, url='/'):
        self.label = label
        self.url = url
        self.tag = ListItem()

    def get_url(self):
        return self.url

    def is_parent(self):
        return False


class NestedGroup(NavItem):

    def __init__(self, label, items, url=None):
        self.label = label
        self.url = url
        self.items = list(items)
        self.tag = UnorderedList()

    @property
    def active(self):
        return any(item.active for item in self.items)

    def is_parent(self):
        return True