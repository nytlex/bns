__author__ = 'Keith Byrne'

from flask import current_app
from app.web.extensions.hypernav.markup.renderer import Renderer
from flask import request


class CommonNav(object):

    def __init__(self, app=None):
        self.render = Renderer()
        self.items = {}
        self.app = app
        self.breadcrumbs = []
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        app.config.setdefault('NAV', True)
        if hasattr(app, 'teardown_appcontext'):
            app.teardown_appcontext(self.teardown)
        else:
            app.teardown_request(self.teardown)

    def register_current_resource(self):
        path = request.path
        for i in self.items:
            if i.url is not None and i.url in path:
                i.active = True
                self.breadcrumbs.append({
                    'label': i.label,
                    'url': i.url
                })
                break

    def generate_markup(self):
        for i in self.items:
            if i.is_parent():
                for j in i.items:
                    j.set_markup(self.render.render_from_attributes(i, i.label))
            i.set_markup(self.render.render_from_attributes(i, i.label))

    def register_item(self, _id, _item):
        self.items[_id] = _item

    def register(self, *_items):
        self.items = _items

    def __iter__(self):
        return iter([i for i in self.items])