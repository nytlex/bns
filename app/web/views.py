import logging

from datetime import timedelta
from datetime import datetime
from flask import jsonify
from flask import render_template
from flask import request
from flask import current_app
from flask import make_response
from flask import Response
from werkzeug.exceptions import HTTPException

from app.errors.exceptions import ServerException
from app.web.filters import datetime_filter
from app.web.models import Inquiry
from app.web.models import News
from app.web import site_controller, limiter
from app.utils.response import JsonOKResponse, JsonOverloadResponse
from app.web.extensions.hypernav.navigation import CommonNav
from app.web.extensions.hypernav.constructs import NestedGroup, Link

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)

nav = CommonNav()
nav.register(
    Link('Home', '/'),
    Link('About', 'about'),
    Link('News', 'news'),
    NestedGroup('Resources', [
        Link('Something', 'something'),
        Link('Something', 'something'),
        Link('Something', 'something'),
        Link('Something', 'something'),
    ], url='resources'),
    Link('Contact', 'contact'),
    Link('Calendar', 'calendar'),
    NestedGroup('Reports', [
        Link('Whole School Evaluation', 'reports')
    ], url='reports')
)


def limit_key_func():
    return current_app.config.get("BOOKING_RATE_LIMITS", "1/minute")


def get_sitemap():
    pages = []
    ten_days_ago = datetime.now() - timedelta(days=10)
    for rule in current_app.url_map.iter_rules():
        if "GET" in rule.methods and len(rule.arguments) == 0 and rule.subdomain == '' and '/admin/' not in rule.rule:
            pages.append([rule.rule, ten_days_ago])
    return pages


def format_datetime(input_string):
    date_format = "%m/%d/%Y"
    return datetime.strptime(input_string, date_format)


def create_template(template: str=None, **kwargs) -> str:
    """
    Utility function to create common application templates including a number of helper attributes.
    This is to aid in mobile first and primarily a function to reduce load times on smaller devices by
    specifying whether it is mobile and thus, requesting mobile friendly assets during rendering.

    :param str template: Template path and name
    :rtype str
    :return rendered template
    """
    nav.register_current_resource()
    nav.generate_markup()
    if not template:
        raise ValueError('A template path and name is required')
    return render_template(
        template,
        navigation=nav,
        mobile=request.user_agent.platform in ["iphone", "android", "blackberry"],
        deployment_hostname=current_app.config['DEPLOYMENT_HOSTNAME'],
        **kwargs
    )


@site_controller.route('/')
def index():
    template = create_template('home.html', lang=None)
    return Response(template)


@site_controller.route('/about', methods=['GET'])
def about():
    template = create_template('pages/about.html')
    return Response(template)


@site_controller.route('/contact', methods=['GET'])
def contact():
    template = create_template('pages/contact.html')
    return Response(template)


@site_controller.route('/reports', methods=['GET'])
def reports():
    template = create_template('pages/reports.html')
    return Response(template)


@site_controller.route('/resources', methods=['GET'])
def resources():
    template = create_template('pages/resources.html')
    return Response(template)


@site_controller.route('/search', methods=['GET', 'POST'])
def search():
    template = create_template('pages/search.html')
    return Response(template)


@site_controller.route('/news', methods=['GET'])
@site_controller.route('/news/article/<int:article_id>', methods=['GET'])
@site_controller.route('/news/article/<string:slug>', methods=['GET'])
@site_controller.route('/news/author/<string:author_name>', methods=['GET'])
def news(article_id=None, slug=None):
    if article_id:
        articles = [News.get_one(article_id)]
    else:
        articles = News.get()
    template = create_template('pages/news.html', articles=articles)
    return Response(template)


@site_controller.route('/sitemap', methods=['GET'])
@site_controller.route('/<lang>/sitemap', methods=['GET'])
@site_controller.route('/sitemap<path:extension>', methods=['GET'])
def sitemap_xml(lang=None, extension=None):
    """
    Generate sitemap.xml. Makes a list of urls and date modified.
    """
    if extension == '.xml':
        response = make_response(render_template(
            'sitemap_template.xml',
            pages=get_sitemap(),
            deployment_hostname=current_app.config['DEPLOYMENT_HOSTNAME']
        ))
        response.headers["Content-Type"] = "application/xml"
        return response
    elif extension == '.json':
        return jsonify(sitemap=get_sitemap())
    return make_template('sitemap.html', lang=lang)


@site_controller.route('/<string:path>')
def catch_invalid_url(path):
    raise ServerException(status_code=404)


@site_controller.errorhandler(429)
def catch_error(error):
    lang = request.view_args['lang'] or None
    return JsonOverloadResponse(
        status=429
    )