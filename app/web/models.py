from datetime import datetime

from sqlalchemy.ext.hybrid import hybrid_property
from flask import render_template
from flask import current_app

from app.database import database as db, DeclarativeBase
from app.web.mailer import send_email


def format_moment_datetime(input_date):
    if type(input_date) != str:
        raise ValueError('Input needs to be a string datetime')
    month = input_date[:2]
    day = input_date[3:5]
    year = input_date[6:]
    return datetime(int(year), int(month), int(day))


class User(db.Model, DeclarativeBase):
    __tablename__ = 'user'
    created = db.Column(db.DateTime())
    username = db.Column(db.String(120), unique=True)
    firstname = db.Column(db.String(120), unique=True)
    surname = db.Column(db.String(120), unique=True)
    password = db.Column(db.Text, nullable=False)
    admin = db.Column(db.String(5))
    position = db.Column(db.String(120), unique=False)
    articles = db.relationship("News", backref=db.backref("news", lazy='joined'))

    @hybrid_property
    def resource_identifier(self):
        resource_format = '{}-{}'
        return resource_format.format(self.firstname.lower(), self.surname.lower())

    @staticmethod
    def find_user(username=None):
        if username:
            return User.query.filter_by(username=username).one_or_none()
        return User.query.all()

    def check_password(self, candidate_password):
        if candidate_password == self.password:
            return True
        return False
        # return check_password_hash(self.password, password)

class Inquiry(db.Model, DeclarativeBase):
    __tablename__ = 'inquiry'
    request_datetime = db.Column(db.DateTime())
    name = db.Column(db.String(120), unique=True)
    email = db.Column(db.String(120), unique=True)
    phone = db.Column(db.String(120), unique=True)
    message = db.Column(db.Text)

    def __init__(self, request_datetime=request_datetime, name=name, email=email, phone=phone, message=message):
        self.request_datetime = request_datetime
        self.name = name
        self.email = email
        self.phone = phone
        self.message = message

    @staticmethod
    def get():
        return Inquiry.query.all()

    def make_email(self):
        template = render_template(
            'mail/contact.html',
            username=self.email,
            message=self.message,
            timestamp=datetime.now()
        )
        return template

    def dispatch_mail(self):
        template = self.make_email()
        send_email([current_app.config['SITE_ADMIN']], 'Contact', template, current_app.config['SERVICE_ADDRESS'])
        return True


class News(db.Model, DeclarativeBase):
    __tablename__ = 'news'
    created = db.Column(db.DateTime())
    title = db.Column(db.String(120), unique=True)
    image = db.Column(db.String(120))
    content = db.Column(db.Text())
    author = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    slug = db.Column(db.String(120), unique=True)
    author_join = db.relationship("User", backref=db.backref("user", lazy='joined'))

    @staticmethod
    def get():
        return News.query.all()

    @staticmethod
    def get_one(article_id):
        return News.query.filter_by(id=article_id).one()

