from flask import Blueprint
from flask_mail import Mail
from flask_limiter.util import get_remote_address
from flask_limiter import Limiter

from app.web.extensions.hypernav.navigation import CommonNav

limiter = Limiter(key_func=get_remote_address)
mail = Mail()

site_controller = Blueprint(
    'web',
    __name__,
    template_folder='../web/templates',
    static_folder='./static',
    static_url_path='/web/static'
)

from app.web import views