import logging

from wtforms import Form
from wtforms import StringField
from wtforms import validators
from wtforms import PasswordField


logger = logging.getLogger(__name__)


class LoginForm(Form):
    username = StringField('Username', [validators.input_required()])
    password = PasswordField('Password', [validators.input_required()])