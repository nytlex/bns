from functools import wraps

from flask import render_template
from flask import request
from flask import Response
from flask import redirect
from flask import current_app
from flask import url_for
from flask import flash

from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature
from itsdangerous import SignatureExpired

from app.admin import admin_controller
from app.admin.forms import LoginForm
from app.web.models import User

AUTH_KEY_NAME = 'nytlex.auth.token'


def generate_auth_token(username):
    s = Serializer(
        current_app.config['SECRET_KEY'],
        expires_in=current_app.config['AUTH_TOKEN_EXPIRY'],
    )
    return s.dumps({
        'username': username
    })


def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = User.find_user(username=data['username'])
        return user


def lookup_auth_token():
    if request.cookies.get(AUTH_KEY_NAME):
        return request.cookies.get(AUTH_KEY_NAME)
    return None


def check_access(func):
    @wraps(func)
    def wrapped_function(*args, **kwargs):
        if lookup_auth_token() is None:
            return redirect(url_for('admin.login'))
        elif verify_auth_token(request.cookies.get(AUTH_KEY_NAME)):
            return func(*args, **kwargs)
        return redirect(url_for('admin.login'))
    return wrapped_function


@admin_controller.route('/')
@check_access
def index():
    return render_template('dashboard.html')


@admin_controller.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html', form=LoginForm())
    form = LoginForm(request.form)
    if form.validate():
        user = User.find_user(username=form.username.data)
        if not user:
            form.username.errors.append('Invalid Username or Password')
            flash('Invalid Username or Password')
            return render_template('login.html', form=form)
        user.check_password(form.password.data)
        response = Response(render_template('dashboard.html'))
        response.set_cookie(AUTH_KEY_NAME, value=generate_auth_token(user.username))
        return response
    return render_template('login.html', form=form)


@admin_controller.route('/logout', methods=['GET'])
def logout():
    if lookup_auth_token():
        try:
            response = redirect(url_for('admin.login'))
            response.set_cookie(AUTH_KEY_NAME, expires=0)
            return response
        except KeyError:
            pass
    return redirect(url_for('admin.login'))



