__author__ = 'Keith Byrne'

from flask import Blueprint
from flask_mail import Mail

# limiter = Limiter(key_func=get_remote_address)
mail = Mail()

admin_controller = Blueprint(
    'admin',
    __name__,
    template_folder='templates',
    static_folder='./static',
    static_url_path='/admin/static'
)

from app.admin import views