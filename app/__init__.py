from flask import Flask
from flask import url_for
from flask import redirect
from flask.ext.cors import CORS
from flask import render_template

from app.errors import error_handler
from app.web import site_controller, limiter, mail
from app.admin import admin_controller
from app.assets import asset_controller
from app.database import database


def verify_release_configuration():
    """
    A pre deployment sanity check to ensure that the minimal required configuration mappings
    are present within the release deploy/config.cfg file.

    Function will look for core config members such as secret key, salts etc...

    :return True / False: True or False if verified or unverified
    :rtype bool
    """
    return False


def create_app(env='DEV'):
    state = Flask(__name__, instance_relative_config=True)
    cors = CORS(state, resources={
        r'/web/static/*': {'origins': '*'}
    })
    config_path = None
    if env == 'DEV':
        config_path = 'app.config.DevelopmentConfig'
    elif env == 'TEST':
        config_path = 'app.config.TestingConfig'
    elif env == 'RELEASE':
        config_path = 'app.config.ReleaseConfig'

    state.config.from_object(config_path)

    state.config.from_pyfile('config.cfg', silent=True)

    database.init_app(state)
    mail.init_app(state)
    limiter.init_app(state)

    state.register_blueprint(asset_controller)
    state.register_blueprint(site_controller)
    state.register_blueprint(admin_controller, url_prefix='/admin')
    state.register_blueprint(error_handler)

    return state