__author__ = 'Keith Byrne'

from flask import Blueprint

asset_controller = Blueprint(
    "asset_controller",
    __name__,
    static_folder='./lib',
    static_url_path='/assets/lib'
)