from flask_sqlalchemy import SQLAlchemy, Model

database = SQLAlchemy()


class DeclarativeBase(Model):
    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    active = database.Column(database.VARCHAR(2), primary_key=False, nullable=True)

    def persist(self):
        database.session.add(self)
        return database.session.commit()

    def delete(self):
        database.session.delete(self)
        return database.session.commit()

    def serialize_model(self, descend_level=False, exclusions=[]):
        """
        This utility function dynamically converts Alchemy model classes into a dict using introspective lookups.
        This saves on manually mapping each model and all the fields. However, exclusions should be noted.
        Such as passwords and protected properties.
        :param descend_level: Whether or not to introspect to FK's
        :return: json data structure of model
        :rtype: dict
        """
        # Define our model properties here. Columns and Schema relationships
        columns = [col for col in self.__mapper__.columns.keys() if col not in exclusions]
        relationships = self.__mapper__.relationships.keys()
        model_dictionary = self.__dict__
        resp = {}
        # First lets map the basic model attributes to key value pairs
        for c in columns:
            resp[c] = model_dictionary[c]
        if descend_level is False or not relationships:
            return resp
        # Now map the relationships
        for r in relationships:
            resp[r] = self.convert_model_to_json(getattr(self, r), False, exclusions)
        resp = resp.__dict__
        return resp


def create():
    database.create_all()